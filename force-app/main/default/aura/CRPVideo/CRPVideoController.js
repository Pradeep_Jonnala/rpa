({
    handleInit: function (component, event) {
         /*parse the recordId from the URL. The record Id is used to share 
          the uploaded file across all the community users */
        let paths = window.location.pathname.split("/");
        component.set("v.recordId", paths[paths.length - 2]);
        console.log('v.recordId in console log='+component.get("v.recordId"));
    },

    handleUploadFinished: function (component, event) {

        // Fire the toast event
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success',
            message: 'File has been uploaded successfully',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();

        var files = component.get("v.files") || [];
        // Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        // Get the file name
        uploadedFiles.forEach(function(file){
            file.videoUrl = '/sfsites/c/sfc/servlet.shepherd/document/download/'+file.documentId;
            files.push(file); 
        });
        component.set("v.files", files);

        var compEvent = component.getEvent("sendChatterExtensionPayload");
        compEvent.setParams({
            "payload" : files,
            "extensionDescription" : "Preview Video files",
            "extensionTitle": "Preview"
        });
        compEvent.fire();
    }
})